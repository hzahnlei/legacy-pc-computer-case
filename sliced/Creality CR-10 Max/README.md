# Slicer Settings

I am using Cura 5.1.0 default settings for Creality CR-10 Max and PLA as a material.
I only slightly modified the defaults for 0.4mm and 0.8mm nozzles.
I am keeping all other parameters unchanged.
Especially nozzle temperature at 200°C and build plate temperature at 60°C.
These are my only changes to the default I am making:

-  Coarse profile (`coarse.curaprofile`) for large objects (0.8mm nozzle):
    - Tree support, touching buildplate
-  Fine profile (`fine.curaprofile`) for small objects (0.4mm nozzle)
    - Tree support, touching buildplate

## Computer Case

| File                                 | Profile       |
| ------------------------------------ | ------------- |
| `Bottom shell (NEU) v12.gcode`       | Coarse, 0.8mm |
| `Chrome rings v10.gcode`             | Fine, 0.4mm   |
| `L logo (rounded) v6.gcode`          | Fine, 0.4mm   |
| `Legacy logo (rounded) v6.gcode`     | Fine, 0.4mm   |
| `Legacy logo (rounded) 75% v6.gcode` | Fine, 0.4mm   |
| `Strap v59.gcode`                    | Fine, 0.4mm   |
| `Top shell (NEU) v20.gcode`          | Coarse, 0.8mm |

## Keyboard Case

| File                                       | Profile       |
| ------------------------------------------ | ------------- |
| `Bottom shell v62.gcode`                   | Coarse, 0.8mm |
| `Keyboard holder v62.gcode`                | Coarse, 0.8mm |
| `Keyboard raiser v2.gcode`                 | Coarse, 0.8mm |
| `Top shell with indicator lamps v80.gcode` | Coarse, 0.8mm |

## Mouse Case

| File                   | Profile       |
| ---------------------- | ------------- |
| `Bottom v15.gcode`     | Coarse, 0.8mm |
| `PCB holder v29.gcode` | Coarse, 0.8mm |
| `Top v29.gcode`        | Coarse, 0.8mm |

## Monitor Case

| File                             | Profile       |
| ---------------------------------| ------------- |
| `Back v56.gcode`                 | Coarse, 0.8mm |
| `Bezel v103.gcode`               | Coarse, 0.8mm |
| `Crate L v103.gcode`             | Coarse, 0.8mm |
| `Crate R v103.gcode`             | Coarse, 0.8mm |
| `Cratemounts v103.gcode`         | Coarse, 0.8mm |
| `Front v19.gcode`                | Coarse, 0.8mm |
| `Outer speaker covers v56.gcode` | Fine, 0.4mm   |
| `Panel support v103.gcode`       | Coarse, 0.8mm |
| `Speaker cover v56.gcode`        | Coarse, 0.8mm |
| `Speaker holders v103.gcode`     | Coarse, 0.8mm |
| `Stand v56.gcode`                | Coarse, 0.8mm |
| `Tray v103.gcode`                | Coarse, 0.8mm |
