# Preparing Indicator Lamps

For this design I did not want to use ordinary LEDs.
At least I wanted them to be not obvious.
I puchase inexpensive and bulky lamps in different colors.

The lamps were meant for 12V.
I cut them in half and removed the 12V lamp in order to replace them with LEDs which can convenientely connected to the motherboard, SSD and keyboard.

![Lamps cut in half](./01_cut_in_half.jpeg "Lamps Cut in Half")

Original lamp, lamp cut in half and LED to be inserted.

![Lamps cut in half](./02_all_parts.jpeg "Lamps Cut in Half")

DIY plug by soldering a pin header to an LED.

![Lamps cut in half](./03_pin_header_soldered.jpeg "Lamps Cut in Half")

LED glued inside the lamp casing with hot glue.

Sadly, some of the lamp casings melted and deformed because of the temperature of the hot glue.

![Lamps cut in half](./04_led_inserted.jpeg "Lamps Cut in Half")

Cable connected to finished lamp/LED.

![Lamps cut in half](./05_cable_connected.jpeg "Lamps Cut in Half")
