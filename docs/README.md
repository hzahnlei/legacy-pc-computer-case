# General Remarks

## Mounting Materials

Most of the time I am using 3x10mm and 3x16mm screws to attach things to each other.
These are self-cutting screws usually intended for wood but they work fine on PLA as well.

On the bottom of the computer case and at the back of the monitor I am using 4x25mm metric screws and brass inserts.

The logos are attached with double-sided adhesive tape.

## 3D Printing Advice

Please note that the parts are quite big and thus require a big 3D printer.
I am using a Creality CR-10 Max.
Alternatively you have to cut them into smaller pieces before slicing.

## Finishing Printed Surfaces

Of cause, the surface of 3D printed objects is not perfect.
It cannot compete with die cast plastic cases.
Be advised that it takes relatively extensive filling and sanding to make it look as smooth and glossy like on the pictures herein.
In fact it required multiple passes of filling and sanding.
Its a real crap job.

Also primers, fillers and colors are relatively expensive.
A good share of the budget went into the finish.

## Pictures Of the Finished Product

Computer, keyboard and mouse.
Monitor not printed yet.

Even though it was my intention to make it look like a piece from the 50s, it also brings associations with 70s soviet nuclear power plants to my mind.
Espesially when the red SSD lamp is flashing.

![Design](./02_everything_but_monitor.jpeg "Design")

Unfinished monitor put together to find out whether everything fits together as planned before putting in the effort of sanding and spray painting.

![Design](./03_raw_monitor.jpeg "Design")

TFT panel mounted to finished bezel.
Front and rar case not finished yet.

![Design](./04_bezel_no_case.jpeg "Design")
