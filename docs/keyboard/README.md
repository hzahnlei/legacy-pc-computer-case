# Keyboard Case

The keyboard case is a simple part, much like the computer case.
It is made up of a top and bottom shell.
The actual keyboard rests on an adapter.
This adapter is exchangable so that the position and fit of the keyboard can be optimized without the need to print a complete case.

## Printing

This picture shows the printing of the upper shell in an early stage.

![Printing the upper shell](./04_print_upper_shell.jpeg "Printing the Upper Shell")

## Finishing

The surface finish required multiple passes of filling and sanding:
-  Putty, sanding with 60 grain
-  Spray putty, sanding with 120 grain
-  Fille, whet sanding with 600 grain
-  Color spray (acryllic)
-  Glossy clear coat spray for sealing and protecting the surface

The hastly assembled raw prints.
The print si coarse grained.
It is easily recognizable that it needs post processing to give an impression.

![Raw print](./05_raw.jpeg "Raw Print")

## Assembling

The USB cable is not soldered to the keyboard PCB.
Instead is is connected with an connector.
This is great as it allows changing the USB cable to suit our needs.

![USB connector on PCB](./01_usb_connector.jpeg "USB Connector on PCB")

The inexpensive keyboard I chose is partially backlit.
The caps lock key and the num pad keys are backlit.

![Num pad LED](./06_num_pad_led.jpeg "Numeric Pad LED")

![Num pad LED](./03_caps_led.jpeg "Numeric Pad LED")

I desoldered the num pad and caps lock LEDs in order to connect my own bulky indicator lamps.

![Num pad LED desoldered](./07_num_pad_led.jpeg "Numeric Pad LED Desoldered")

![New num pad LED soldered](./08_num_pad_led.jpeg "New Numeric Pad LED Soldered")

You should see nice glowing indicator lamps, if everything goes right.

![Successfully installed LEDs](./09_leds_glowing.jpeg "Successfully Installed LEDs")

The keyboard case assembly is quite simple:
1.  Attach the keyboard adapter to the bottom half.
2.  Lay the keyboard into the adapter.
    Carefully place the cables so that they are not crushed or cut.
3.  Place the top half onto the bottom half so that the keys fit through the openings in the top half.
4.  Turn the whole assembly upside down and screw both halfes together from the back.