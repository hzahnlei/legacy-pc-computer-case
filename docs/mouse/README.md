# Mouse Case

Design of the mouse case was quite challenging.
I am using an inexpensive standard office mouse from Fujitsu.
The chellange was to design the interior of the mouse case so that the PCB and scroll wheel fit inside.
Both should be helt in place reliably.

A new and a seasoned Fujitsu mice side by side.

![Standard office mouse](./01_old_and_new.jpeg "Standard Office Mouse")

## Printing

I needed multiple attempts to get the design right.
Each iteration was printed and then refined.

![Test prints](./02_test_prints.jpeg "Test Prints")

## Finishing

The surface finish required multiple passes of filling and sanding:
-  Putty, sanding with 60 grain
-  Spray putty, sanding with 120 grain
-  Fille, whet sanding with 600 grain
-  Color spray (acryllic)
-  Glossy clear coat spray for sealing and protecting the surface

It is clear that this print requires sanding.

![Sanding required](./04_raw.jpeg "Sanding Required")

The finished product features a smooth surface.
The "L" logo is helt in place by adhesive tape again.

![Finished](./05_painted.jpeg "Finished")

## Assembling

1. The PCB is mouted to the bottom half.
   It is helt in place by four small brackets.
   These are screwed down by 3x10mm short screws.
2. Next the scroll wheel is put into its place.
3. The upper shell is imposed on the bottom half.
   Make sure not to crush or damage any wires.
4. Flip it over and make both parts hold together by four 3x10mm or 3x16mm screws.

![PCB mounted to bottom case](./03_mounting.jpeg "PCB Mounted to Bottom Case")
