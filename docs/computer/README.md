# Computer Case

The computer case is actually the simplest part.
It is made up of a lower and an upper shell.
The naked Mini-ITX cage (holding mainboard, power supply unit and drives) is sandwitched between those two halfes.

## Printing

The following picture shows the printing of the bottom computer case.
Just ignore the stringing of my printer.
This may not happen if you have set up your printer and slicer profiles better than mine.

![Printing the lower shell](./01_print_lower_shell.jpeg "Printing the Lower Shell")

After printing the bottom shell I was inserting the Mini-ITX cage to see whether it actually fits.
Please note the rough surface.
I am using a 0.8mm nozzle for big parts.
The surface is coarse grained accordingly.
It requires filling and sanding to achieve a smooth and glossy surface.

![Checking the accuracy of fit](./02_preview.jpeg "Checking the Accuracy of Fit")

![Unfinished case put together to get a picture](./03_raw_case.jpeg "Unfinished Case Put Together to Get A Picture")

## Finishing

The surface finish required multiple passes of filling and sanding:
-  Putty, sanding with 60 grain
-  Spray putty, sanding with 120 grain
-  Fille, whet sanding with 600 grain
-  Color spray (acryllic)
-  Glossy clear coat spray for sealing and protecting the surface

The case after filling, sanding and spray painting.

![Painted computer case](./03_painted_shell.jpeg "Painted Computer Case")

## Assembling

Buttons and lamps came without chrome rings.
I wanted them because I thought this suits the design.
The chrome rings are printed with a finer 0.4mm nozzle and spray painted with chrome effect color.
They are not chrome plated.

![Chrome rings for buttons and lamps](./04_button_and_lamps.jpeg "Chrome Rings for Buttons and Lamps")

Inside the case are two straps that are glued and screwed to the top half.
It is then screwed with M4 screws on the bottom.
This holds both halfes together and additionally pinches the Mini-ITX case.

![Strap connecting top and bottom halfes](./05_strap.jpeg "Strap Connecting Top and Bottom Halfes")

I attched the printed logo to a strip of double-sided adhesive tape.
The access tape was remove with a hobby knife.

![Logo with adhesive tape](./06_logo.jpeg "Logo with Adhesive Tape")

The logo is attached to the case with double-sided adhesive tape.

![Logo attached to case](./07_front.jpeg "Logo Attached to Case")

The Mini-ITX cage rests in its location on the bottom half of the case.

On the upper half of the case one may regognize power button and indicator lamps.
On the lowwe half of the picture one sees the I/O panel screwed to the case.
The I/O panel was salvaged from the original case.

![Mini-ITC cage in case](./08_wiring.jpeg "Mini-ITC Cage In Case")

I cut the wires from button and LEDs of the original case.
I added small jacks made of pin headers.
Pin headers are soldered to the LEDs.
This way, the lamps can be inserted from the outside.
The are then secured from the inside with nuts.
Then, they are connected to the motherboard by plugging the cables and the button and LEDs.

![Mini-ITC cage in case](./09_lamps.jpeg "Mini-ITC Cage In Case")

On the bottom of the lower shell I added self-adhesive felt pads.
Also, recognize the hole in the bottom.
This helped saving printing time and filament.
It also serves as a venting hole.
There is still some space left so that a thin styrene sheet or metal grid can be used to close the hole, if desired.

![Venting hole](./10_bottom.jpeg "Venting Hole")

The next picture shows the power and HDD indicator lamps in action.

![Venting hole](./11_lamps_glowing.jpeg "Venting Hole")

The back of the case revealse the back of the Mini-ITX cage.
All connectors of the Mini-ITX board and power supply are easily accessible.
Venting holes are not covered on order not to hinder cooling.

![Opening at the back of the case](./12_rear.jpeg "Opening At the Back Of the Case")

The I/O panel from the original case is accessible at the front right of the case.

![I/O panel](./13_extra_usb_ports.jpeg "I/O Panel")
