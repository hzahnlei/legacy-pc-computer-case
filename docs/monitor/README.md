# Monitor Case

I had an old 17" TFT monitor with VGA port sitting on my shelf.
It also had built-in speakers which made it ideal for my project.
For the design of the monitor case I first disassebled the monitor to take measurements and develop ideas and solutions.
A 17" panel is a relatively big panel and requires a big case.
The design needs to take printability into account.
The part should not be too big and yet should look like a real vintage monitor.

This is the original control panel of the TFT monitor.

![Original control panel](./01_original_control_pannel.jpeg "Original Control Panel")

The TFT panel, speakers and control panel PCB are becomming visible once the front case has been removed.

![The internals laying open](./02_front_case_removed.jpeg "The Internals Laying Open")

![Close-up of speakers](./03_speaker.jpeg "Close-up of Speakers")

The monitor is controllable by the user via buttons at the front of the original case.
Behind the plastic facade sits a PCB with micro switches and a power indicator LED.
The LED is bi-color with common ground.

![Original control panel PCB (front view)](./orig_panel_front.png "Original Control Panel PCB (Front View)")

![Original control panel PCB (rear view)](./orig_panel_back.png "Original Control Panel PCB (Rear View)")

The following diagram shows the pinout of the pin header located on the PCB as well as the wiring of the bi-color LED.

![Original control panel pin headers](./pin_header.svg "Original Control Panel PCB Pin Headers")

Initially my plan was to remove the PCB and make my own.
I simply wanted my extra bulky buttons and lamps to connect to a pin header on a small prototyping board.
The jack from the TFT would then plug into these pin headers.

At least this was my idea until I recognized that the cable comming from the TFT panel ends in a pin header with an 2.0mm raster.
As a hobby tinkerer I do only have 2.54mm raster pin headers and prototyping PCBs in my drawer.
Therefore I decided to connect my buttons and indicator lamps to the original PCB instead.

## Printing

The monitor case was the most complex part to design but also to print.
Especially the back shell was challenging due to its size and required supports.

![Printing the back shell](./07_print_back_shell.jpeg "Printing the Back Shell")

The venting holes of the back were especialy challenging.
The support structure went wrong.
So I added piece of fillamte, fixed with glue to make sure the next layers of the support would stabilize.

I made the covers of the venting holes too thin.
I think this was a mistake.
This has been fixed only after the print was done.
The covers are now 2mm not 1mm and should be more stable and print easier.

![Stabilizing supports with pieces of fillament and glue](./05_stabilizing_support_with_glue.jpeg "Stabilizing Supports with Pieces of Fillament and Glue")

![Stabilizing supports with pieces of fillament and glue](./06_stabilizing_support_with_glue.jpeg "Stabilizing Supports with Pieces of Fillament and Glue")

## Finishing

The surface finish required multiple passes of filling and sanding:
-  Putty, sanding with 60 grain
-  Spray putty, sanding with 120 grain
-  Fille, whet sanding with 600 grain
-  Color spray (acryllic)
-  Glossy clear coat spray for sealing and protecting the surface

## Assembling

### Bezel and TFT Panel

The heavy TFT panel features only four mounting holes for relatively small screws.
Therefore I designed add retaining clamps to hold the panel.

![Additional holding clamps](./04_holding_clamp.jpeg "Additional Holding Clamps")

Lay the front part, the bezel, upside down on a table.

![Bezel laying upside down](./10_mount_bezel.jpeg "Bezel Laying Upside Down")

Now, lay the TFT panel, also upside down, on top of the bezel.
Align all four mounting holes of the panel and the bezel.
Then screw down the panel to the bezel quite tight.

![Panel mounted to bezel](./11_mount_panel.jpeg "Panel Mounted to Bezel")

The panel is relatively heavy.
Therefore I designd a support to be mounted to the bottom of the bezel.
This should give the panel additional hold.

Note that the bezel is not that thick.
I used 10mm screws to avoiud them puncture the visible side of the bezel.

![Panel support](./12_mount_bottom_support.jpeg "Panel Support")

![Panel support](./13_mount_bottom_support.jpeg "Panel Support")

Now mount the crate to the bezel.
The crate features lateral holding clamps.
It is screwed to the besel at both sides.
Here I used 16mm screws.

![Panel support](./14_mount_crate.jpeg "Panel Support")

Speaker holders are mounted to the crate.
Speakers are helt in place by 3mm metric screws.

![Speaker holders](./15_mount_speakers.jpeg "Speaker Holders")

The purpose of the crate is manyfold:
-  It is supposed to give the whole structure stability.
-  The pwer supply can be attached to it.
-  It connects front and back of the case and holds them together by use of 4mm metric screws.

![Panel support](./16_mounted_interior.jpeg "Panel Support")

### Front Frame

Now it is time to insert the cage with bezel and panel into the front frame.
For this we need to first mount the buttons and indicator lamps to the front.
We also need to wire the buttons and lamps to the original PCB.

First I desoldered the power/standby LED.

![LED desoldered](./17_led_desoldered.jpeg "LED Desoldered")

Next, buttons and lamps (LEDs) are wired to the PCB.

![Buttons and LEDs soldered to PCB](./18_buttons_soldered_to_pcb.jpeg "Buttons and LEDs Soldered to PCB")

The result was a mess of wires.

![Mess of wires](./19_buttons_soldered_to_pcb.jpeg "Mess of Wires")

A closeup of the indicator lamps show how the LEDs were fixed inside the lamp housing with hot glue.

![Indicator lamps](./20_indicator_lamps.jpeg "Indicator Lamps")

The next step was to harness the mess of wires by tying them together to thicker stands.

![Harnessing the wires](./21_harnessing_the_wires.jpeg "Harnessing the Wires")

Next, the bezel with TFT panel and cage is inserted into the front frame/case.
It is screwed down with six 3x16mm screws.

![Harnessing the wires](./22_mounted_to_front.jpeg "Harnessing the Wires")

I was recycling the original PCB as I could not use a custom made PCB (2mm raster of original pin header connectors).
So I had to mount it to the cage in an improvised way.
The 3D print contains mounting holes for the custom PCB but not for the original PCB.
For heavens sake, the wood screews are self-cutting their way into the PLA with ease.

![Mounting PCB to cage](./23_pcb_mounted_to_cage.jpeg "Mounting PCB to Cage")

### Back Case

We now need to prepare the back case so that it can be mounted to the front frame.
Various steps need to be taken first to prepare the back case.

In the fities, speakers were often covered with cloth/fabric.
I just took a simple, white cloth.
It should not be too thick in order not to hinder the sound waves to travel through it.

![Cloth for covering the speakers](./26_speaker_cover.jpeg "Cloth for Covering the Speakers")

![Speaker covers mounted to back case](./27_speaker_cover.jpeg "Speaker Covers Mounted to Bback Case")

As a final add-on part we are mounting a stand at the bottom of the back case.

![Stand](./28_stand.jpeg "Stand")

Therefore I used nine 3x10mm screws.

![Mounting stand to back case](./29_mounting_stand_to_back.jpeg "Mounting Stand to Back Case")

The PSU is strapped to the cage by means of cable ties.
Also the cables are fixed with cable ties as kind of a strain relief.

![Cable ties as strain reliefs](./30_psu.jpeg "Cable Ties as Strain Reliefs")

Finally we are imposing the back case onto the front frame.
Just let the cables exit the case through the large opening in the back case.
This opening is meant for venting but also as a cable penetration.

![Imposing back on front](./31_back_integrated.jpeg "Imposing Back on Front")

Back and front are helt together by eight 4x25mm metric screws.
This picture also demonstrates the glossyness of the spray painted case.

![Imposing back on front](./32_glossy_back.jpeg "Imposing Back on Front")

Now, add some self-adhesing felt pads to the stand.
This will prevent scratces in on the top of the computer case in case you place the monitor there.

![Felt pads attached to stand](./33_felt_pads.jpeg "Felt Pads Attached to Stand")

I used transfer letters to lable various buttons and indicator lamps.

I added the labels after integrating the case.
I guess it would have been easier to add the transfer letters before integrating the parts. 

![Mounting PCB to cage](./24_transfer_symbols.jpeg "Mounting PCB to Cage")

![Labeling buttons](./25_labeling.jpeg "Labeling Buttons")

Finally it was done!

![Done](./34_done.jpeg "Done")

![Done](./35_done.jpeg "Done")

The venting holes are supposed to convey the idea that the case was formed from sheet metal.

The picture reveals that the surfaces are by no means perfect - despite my sanding efforts.
Most parts actually turned out quite satisfyingly, however especially the finish of the monitor's large back case was a demanding job.

![Done](./36_venting_holes.jpeg "Done")

