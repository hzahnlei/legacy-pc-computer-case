# The Legacy Computer Case

This project is about 3D printing and assembling a fancy Mini-ITX computer case.
The design lends itself to designs and colors from the 50s.
The case is supposed to look as it was formed from sheet metal.

This is a non-commecial, open source project.
Feel free to print it yourself or to modify it to your liking.

2022-07-21, Holger Zahnleiter

![Banner](./artwork/collage.png "Banner")

## About this Project

I had a somewhat older Mini-ITX PC with a passively cooled Intel Atom dual core CPU wasting space in my hobby room.
Before that it was driving one of my homebrew robots and was replaced by a more capable Nvidia Jetson Nano.
I was almost about to dispose it of when it came to my attention that this thing has legacy interfaces.
It features a serial interface and a printer port.
This is becomming handy every now and then when doing embedded projects.
So, instead of disposing it, I should keept it for embedded projects.

However, the case was an inexpensive one.
It does the job but is otherwise not very stylish let alone an eye-catcher.
Suddenly an idea came to my mind: _"What if I would print a new, exiting case for it?"_

I had a vision of an old 50s Cadillac or fridge.
Those colors like mint green or baby blue combined with chrome.
I wanted it to look like it was formed from sheet metal.
It should be painted in a glossy mint green with some chrome applications.
Furtheremore I wanted bulky indicator lights and buttons.
And it would not be complete without keyboard, mouse and monitor - all styled in the same way.

The name is not derived by the fact that it is looking vintage.
It is it's legacy interfaces which are giving it it's name.
Furthermore it runs Windows 7 to use the legacy programmer software.
Sadly neither Windows XP nor 98 could be installed on this "modern" Intel Atom computer.

## What Is In This Repo

Herein you will find:
-  [Documentation](./docs/) and assembly instructions
-  The original [Fusion 360 files](./cad%20files/)
-  [Step](./exported%20cad%20files/step/) and [STL](./exported%20cad%20files/stl/) exports
-  G-code files pre-sliced for Creality [CR-10 Max](./sliced/Creality%20CR-10%20Max/) printers
-  A [cost report](./cost%20report.numbers) listing the materials used and the associated cost

## Documentation

-  [General Remarks](./docs/README.md)
   -  Mounting materials
   -  3D printing advise
   -  Finishing printed surfaces
   -  Pictures of finished product
-  [Computer Case](./docs/computer/README.md)
   -  Printing, finishing and assembly
-  [Keyboard Case](./docs/keyboard/README.md)
   -  Printing, finishing and assembly
-  [Monitor Case](./docs/monitor/README.md)
   -  Printing, finishing and assembly
-  [Mouse Case](./docs/mouse/README.md)
   -  Printing, finishing and assembly
-  [Indicator Lamps](./docs/lamps/README.md)
   - Repurposing 12V signal lamps
-  Slicing/Printing
   -  Creality CR-10 Max
      - [Slicer Settings](./sliced/Creality%20CR-10%20Max/README.md): In general and per file
      - [Profile for Big Parts](./sliced/Creality%20CR-10%20Max/coarse.curaprofile) (0.8mm nozzle)
      - [Profile for Small Parts](./sliced/Creality%20CR-10%20Max/fine.curaprofile) (0.4mm nozzle)

## Credits

This project is only possible because of the availability of free (somtimes also open source) software and services:
-  The CAD design has been done using the free version of Autodesk's [Fusion 360](https://www.autodesk.de/products/fusion-360/)
-  This Git repository is hosted on [GitLab](https://gitlab.com)
-  I am using [Cura](https://ultimaker.com/de/software/ultimaker-cura) for slizing the 3D models for printing
